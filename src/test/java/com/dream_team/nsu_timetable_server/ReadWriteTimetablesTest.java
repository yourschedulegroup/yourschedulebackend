package com.dream_team.nsu_timetable_server;

import com.dream_team.nsu_timetable_server.model.*;
import com.dream_team.nsu_timetable_server.service.FillTimetableService;
import com.dream_team.nsu_timetable_server.service.TimetableRequestsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.*;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.*;

@SpringBootTest(value = "SpringBootTest.WebEnvironment.MOCK", classes = NsuTimetableServerApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:integration-test.properties")
@ActiveProfiles("test")
class ReadWriteTimetablesTest {

    @Autowired FillTimetableService fillTimetableService;
    @Autowired TimetableRequestsService timetableRequestsService;

    @Test
    @Transactional
    void testGroupsFillingAndReading() {
        var g10 = new Group(18210, 3);
        var g9 = new Group(18209, 3);
        var g3 = new Group(19203, 2);
        var g1 = new Group(20201, 1);
        fillTimetableService.saveGroups(List.of(g1, g3, g9, g10));
        var groups = timetableRequestsService.getGroups();
        assertEquals(4, groups.size());
        assertTrue(groups.containsAll(List.of(g1, g3, g9, g10)));
    }

    @Test
    @Transactional
    void testSpecCoursesFillingAndReading() {
        var g10 = new Group(18210, 3);
        var spec1 = new SpecCourse("TestSpec1", 1, 3);
        var spec2 = new SpecCourse("TestSpec2", 2, 3);
        var spec3 = new SpecCourse("TestSpec3", 1, 3);
        fillTimetableService.saveGroups(List.of(g10));
        fillTimetableService.saveSpecCourses(List.of(spec1, spec2, spec3));
        var specCourses = timetableRequestsService.getSpecCourses(g10.getGroupNumber());
        assertTrue(specCourses.containsKey(1));
        assertTrue(specCourses.containsKey(2));
        assertTrue(specCourses.get(1).containsAll(List.of(spec3, spec1)));
        assertTrue(specCourses.get(2).contains(spec2));
    }

    @Test
    @Transactional
    void testGroupTimetableFillingAndReading() {
        var g10 = new Group(18210, 3);
        var g9 = new Group(18209, 3);
        fillTimetableService.saveGroups(List.of(g10, g9));
        Map<Group, List<TimetableRecord>> timetable = new HashMap<>();
        List<TimetableRecord> tt10 = new ArrayList<>();
        List<TimetableRecord> tt9 = new ArrayList<>();
        // Fill timetable lists

        var lecture = new TimetableRecord(
                new TimetableCell(
                        1,
                        DayOfWeek.MONDAY,
                        LocalTime.of(9, 0),
                        LocalTime.of(10, 35),
                        Week.ALL),
                new Lesson(
                        LessonType.LECTURE,
                        "TestLecture",
                        "TestTeacher1",
                        "12000",
                        "Building"));
        var seminar = new TimetableRecord(
                new TimetableCell(
                        2,
                        DayOfWeek.MONDAY,
                        LocalTime.of(10, 50),
                        LocalTime.of(12, 25),
                        Week.ALL),
                new Lesson(
                        LessonType.SEMINAR,
                        "TestSeminar",
                        "TestTeacher2",
                        "12020",
                        "Building2"));

        tt10.add(lecture);
        tt10.add(seminar);
        tt9.add(lecture);
        timetable.put(g10, tt10);
        timetable.put(g9, tt9);

        fillTimetableService.saveGroupTimetable(timetable);
        var realTimetable10 = timetableRequestsService.getOnlyGroupTimetable(g10.getGroupNumber());
        assertEquals(2, realTimetable10.size());
        assertTrue(realTimetable10.containsAll(List.of(lecture, seminar)));
        var realTimetable9 = timetableRequestsService.getOnlyGroupTimetable(g9.getGroupNumber());
        assertEquals(1, realTimetable9.size());
        assertTrue(realTimetable9.contains(lecture));
    }

    @Test
    @Transactional
    void testGroupTimetableWithSpecCoursesListFillingAndReading() {
        var g10 = new Group(18210, 3);
        var g9 = new Group(17209, 4);
        fillTimetableService.saveGroups(List.of(g10, g9));
        Map<Group, List<TimetableRecord>> timetable = new HashMap<>();
        List<TimetableRecord> tt10 = new ArrayList<>();
        List<TimetableRecord> tt9 = new ArrayList<>();

        // Group lessons
        var lecture = new TimetableRecord(
                new TimetableCell(
                        1,
                        DayOfWeek.MONDAY,
                        LocalTime.of(9, 0),
                        LocalTime.of(10, 35),
                        Week.ALL),
                new Lesson(
                        LessonType.LECTURE,
                        "TestLecture",
                        "TestTeacher1",
                        "12000",
                        "Building"));
        var seminar = new TimetableRecord(
                new TimetableCell(
                        2,
                        DayOfWeek.MONDAY,
                        LocalTime.of(10, 50),
                        LocalTime.of(12, 25),
                        Week.ALL),
                new Lesson(
                        LessonType.SEMINAR,
                        "TestSeminar",
                        "TestTeacher2",
                        "12020",
                        "Building2"));
        // Spec courses
        var specCourse1 = new SpecCourse("Spec1", 1, 3);
        var specCourse2 = new SpecCourse("Spec2", 5, 4);

        fillTimetableService.saveSpecCourses(List.of(specCourse1, specCourse2));

        // Spec courses lessons
        var spec1Lesson = new TimetableRecord(
                new TimetableCell(
                        2,
                        DayOfWeek.WEDNESDAY,
                        LocalTime.of(10, 50),
                        LocalTime.of(12, 25),
                        Week.ALL),
                new Lesson(
                        LessonType.SEMINAR,
                        "Spec1Seminar",
                        "TestTeacher3",
                        "12020",
                        "Building2"));
        var spec2Lesson = new TimetableRecord(
                new TimetableCell(
                        2,
                        DayOfWeek.FRIDAY,
                        LocalTime.of(10, 50),
                        LocalTime.of(12, 25),
                        Week.ALL),
                new Lesson(
                        LessonType.SEMINAR,
                        "Spec2Seminar",
                        "TestTeacher4",
                        "1212",
                        "Building1"));

        tt10.add(lecture);
        tt10.add(seminar);
        tt9.add(lecture);
        timetable.put(g10, tt10);
        timetable.put(g9, tt9);

        Map<SpecCourse, Set<TimetableRecord>> specCoursesTimetable = new HashMap<>();
        specCoursesTimetable.put(specCourse1, Set.of(spec1Lesson));
        specCoursesTimetable.put(specCourse2, Set.of(spec2Lesson));
        fillTimetableService.saveSpecCoursesTimetable(specCoursesTimetable);

        fillTimetableService.saveGroupTimetable(timetable);
        var timetableWithSpec10 = timetableRequestsService.getGroupTimetable(g10.getGroupNumber());
        assertEquals(2, timetableWithSpec10.getTimetable().size());
        assertTrue(timetableWithSpec10.getTimetable().containsAll(List.of(lecture, seminar)));
        assertTrue(timetableWithSpec10.getSpecCourses().get(1).contains(specCourse1));

        var timetableWithSpec9 = timetableRequestsService.getGroupTimetable(g9.getGroupNumber());
        assertEquals(1, timetableWithSpec9.getTimetable().size());
        assertTrue(timetableWithSpec9.getTimetable().contains(lecture));
        assertTrue(timetableWithSpec9.getSpecCourses().get(5).contains(specCourse2));
    }

    @Test
    @Transactional
    void testSpecCoursesTimetableFillingAndReading() {
        var g10 = new Group(18210, 3);
        var g9 = new Group(18209, 3);
        fillTimetableService.saveGroups(List.of(g10, g9));
        var specCourse1 = new SpecCourse("Spec1", 1, 3);
        var specCourse2 = new SpecCourse("Spec2", 2, 3);
        var specCourse3 = new SpecCourse("Spec3", 2, 3);

        fillTimetableService.saveSpecCourses(List.of(specCourse1, specCourse2, specCourse3));

        var spec1Lecture = new TimetableRecord(
                new TimetableCell(
                        1,
                        DayOfWeek.MONDAY,
                        LocalTime.of(9, 0),
                        LocalTime.of(10, 35),
                        Week.ALL),
                new Lesson(
                        LessonType.LECTURE,
                        "Spec1Lecture",
                        "TestTeacher1",
                        "12000",
                        "Building"));

        var spec1Seminar = new TimetableRecord(
                new TimetableCell(
                        2,
                        DayOfWeek.MONDAY,
                        LocalTime.of(10, 50),
                        LocalTime.of(12, 25),
                        Week.ALL),
                new Lesson(
                        LessonType.SEMINAR,
                        "Spec1Seminar",
                        "TestTeacher2",
                        "12020",
                        "Building2"));

        var spec2Seminar = new TimetableRecord(
                new TimetableCell(
                        2,
                        DayOfWeek.MONDAY,
                        LocalTime.of(10, 50),
                        LocalTime.of(12, 25),
                        Week.ALL),
                new Lesson(
                        LessonType.SEMINAR,
                        "Spec2Seminar",
                        "TestTeacher2",
                        "12020",
                        "Building2"));

        var spec3Seminar = new TimetableRecord(
                new TimetableCell(
                        3,
                        DayOfWeek.FRIDAY,
                        LocalTime.of(12, 40),
                        LocalTime.of(14, 5),
                        Week.ALL),
                new Lesson(
                        LessonType.SEMINAR,
                        "Spec3Seminar",
                        "TestTeacher2",
                        "12020",
                        "Building2"));

        Map<SpecCourse, Set<TimetableRecord>> specCoursesTable = new HashMap<>();
        specCoursesTable.put(specCourse1, Set.of(spec1Lecture, spec1Seminar));
        specCoursesTable.put(specCourse2, Set.of(spec2Seminar));
        specCoursesTable.put(specCourse3, Set.of(spec3Seminar));

        fillTimetableService.saveSpecCoursesTimetable(specCoursesTable);

        var allSpecTimetable =
                timetableRequestsService.getSpecTimetable(List.of(
                        specCourse1.getId(), specCourse2.getId(), specCourse3.getId()
                ));

        assertEquals(4, allSpecTimetable.size());
        assertTrue(allSpecTimetable.containsAll(List.of(spec1Lecture, spec1Seminar, spec2Seminar, spec3Seminar)));

        var spec1Timetable = timetableRequestsService.getSpecTimetable(specCourse1.getId());
        var spec2Timetable = timetableRequestsService.getSpecTimetable(specCourse2.getId());
        var spec3Timetable = timetableRequestsService.getSpecTimetable(specCourse3.getId());

        assertEquals(2, spec1Timetable.size());
        assertEquals(1, spec2Timetable.size());
        assertEquals(1, spec3Timetable.size());

        assertTrue(spec1Timetable.containsAll(List.of(spec1Lecture, spec1Seminar)));
        assertTrue(spec2Timetable.contains(spec2Seminar));
        assertTrue(spec3Timetable.contains(spec3Seminar));
    }

    @Test
    @Transactional
    void clearTablesTest() {
        var g10 = new Group(18210, 3);
        var spec1 = new SpecCourse("TestSpec1", 1, 3);
        var spec2 = new SpecCourse("TestSpec2", 2, 3);
        var spec3 = new SpecCourse("TestSpec3", 1, 3);

        fillTimetableService.saveGroups(List.of(g10));
        fillTimetableService.saveSpecCourses(List.of(spec1, spec2, spec3));

        var spec1Lecture = new TimetableRecord(
                new TimetableCell(
                        1,
                        DayOfWeek.MONDAY,
                        LocalTime.of(9, 0),
                        LocalTime.of(10, 35),
                        Week.ALL),
                new Lesson(
                        LessonType.LECTURE,
                        "Spec1Lecture",
                        "TestTeacher1",
                        "12000",
                        "Building"));

        var seminar = new TimetableRecord(
                new TimetableCell(
                        2,
                        DayOfWeek.MONDAY,
                        LocalTime.of(10, 50),
                        LocalTime.of(12, 25),
                        Week.ALL),
                new Lesson(
                        LessonType.SEMINAR,
                        "TestSeminar",
                        "TestTeacher2",
                        "12020",
                        "Building2"));

        Map<SpecCourse, Set<TimetableRecord>> specCoursesTable = new HashMap<>();
        specCoursesTable.put(spec1, Set.of(spec1Lecture));

        fillTimetableService.saveSpecCoursesTimetable(specCoursesTable);

        Map<Group, List<TimetableRecord>> timetable = new HashMap<>();
        List<TimetableRecord> tt10 = new ArrayList<>();
        tt10.add(seminar);
        timetable.put(g10, tt10);

        fillTimetableService.saveGroupTimetable(timetable);

        fillTimetableService.clearAll();

        assertTrue(timetableRequestsService.getGroups().isEmpty());

        assertThrows(RuntimeException.class,
                () -> timetableRequestsService.getSpecTimetable(spec1.getId()));
        assertThrows(RuntimeException.class,
                () -> timetableRequestsService.getSpecCourses(g10.getGroupNumber()));
        assertThrows(RuntimeException.class,
                () ->  timetableRequestsService.getOnlyGroupTimetable(g10.getGroupNumber()));
        assertThrows(RuntimeException.class,
                () -> timetableRequestsService.getGroupTimetable(g10.getGroupNumber()));
        assertThrows(RuntimeException.class,
                () -> timetableRequestsService.getSpecTimetable(List.of(spec1.getId())));
    }
}
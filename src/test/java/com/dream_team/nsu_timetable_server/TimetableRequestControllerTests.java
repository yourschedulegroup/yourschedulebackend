package com.dream_team.nsu_timetable_server;

import com.dream_team.nsu_timetable_server.model.Group;
import com.dream_team.nsu_timetable_server.model.SpecCourse;
import com.dream_team.nsu_timetable_server.service.FillTimetableService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(value = "SpringBootTest.WebEnvironment.MOCK", classes = NsuTimetableServerApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:integration-test.properties")
@ActiveProfiles("test")
class TimetableRequestControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    FillTimetableService fillTimetableService;

    @Test
    @Transactional
    void testGettingGroups() throws Exception {
        var g10 = new Group(18210, 3);
        var g9 = new Group(18209, 3);
        var g3 = new Group(19203, 2);
        var g1 = new Group(20201, 1);
        var groupsList = List.of(g1, g3, g9, g10);
        fillTimetableService.saveGroups(groupsList);

        var groupsJson = mockMvc.perform(get("/api/get/groups"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertTrue(
                new ObjectMapper()
                        .readValue(groupsJson, new TypeReference<List<Group>>() {
                        })
                        .containsAll(groupsList)
        );
    }

    @Test
    @Transactional
    void testGettingSpecCourses() throws Exception {

        var g10 = new Group(18210, 3);
        var spec1 = new SpecCourse("TestSpec1", 1, 3);
        var spec2 = new SpecCourse("TestSpec2", 2, 3);
        var spec3 = new SpecCourse("TestSpec3", 1, 3);
        fillTimetableService.saveGroups(List.of(g10));
        fillTimetableService.saveSpecCourses(List.of(spec1, spec2, spec3));

        var specJson = mockMvc.perform(
                get("/api/get/spec-courses")
                        .param("groupNumber", String.valueOf(g10.getGroupNumber())
                        ))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        var specCourses = new ObjectMapper()
                .readValue(specJson, new TypeReference<Map<Integer, List<SpecCourse>>>() {
                });

        assertTrue(specCourses.containsKey(1));
        assertTrue(specCourses.containsKey(2));
        assertTrue(specCourses.get(1).containsAll(List.of(spec3, spec1)));
        assertTrue(specCourses.get(2).contains(spec2));
    }

}